
var album=['meoAnhLD.png','meoBaTu.png','meoBenga.png','meoMunchkin.png'];
var i=0, count=1;
var seconds=0;  
function displayTimeleft(){
    var minutes = Math.floor(seconds/60);
    var remainderSeconds= seconds%60;
    var display=`<span>${minutes}</span><span>${remainderSeconds< 10 ? '0' : ''}${remainderSeconds}</span>`;
    document.querySelector('#tiles').innerHTML=display;
    seconds--;
    if(seconds<0){
    clearInterval(stop);
    document.querySelector('h3').innerHTML='Time is up!!!'
    }
}
  
        var stop;


        function allowDrop(ev) {
            seconds=12*60*60;
            clearInterval(stop);
            stop= setInterval(displayTimeleft,1000);
            ev.preventDefault();
          }
          
          function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
          }
          
          function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
          }

function nextImg(){
    if(i==album.length-1){
        i=0;
        count=1;
    }
    else{
        i++;
        count++;
    }
    document.querySelector('#banner').querySelector('.container').style.background=`url("img/${album[i]}")no-repeat 0 0px `;   
    document.querySelector('#banner').querySelector('.container').style.backgroundSize='100%';
    var j=i; 
}
    setInterval(nextImg,3000);

    var img = new Image();

    // User Variables - customize these to change the image being scrolled, its
    // direction, and the speed.
    
    img.src = './img/box.png';
    var CanvasXSize = 1400;
    var CanvasYSize = 400;
    var speed = 30; // lower is faster
    var scale = 1.05;
    var y = -4.5; // vertical offset
    
    // Main program
    
    var dx = 0.75;
    var imgW;
    var imgH;
    var x = 0;
    var clearX;
    var clearY;
    var ctx;
    
    img.onload = function() {
        imgW = img.width * scale;
        imgH = img.height * scale;
        
        if (imgW > CanvasXSize) {
            // image larger than canvas
            x = CanvasXSize - imgW;
        }
        if (imgW > CanvasXSize) {
            // image width larger than canvas
            clearX = imgW;
        } else {
            clearX = CanvasXSize;
        }
        if (imgH > CanvasYSize) {
            // image height larger than canvas
            clearY = imgH;
        } else {
            clearY = CanvasYSize;
        }
        
        // get canvas context
        ctx = document.getElementById('canvas').getContext('2d');
     
        // set refresh rate
        return setInterval(draw, speed);
    }
    
    function draw() {
        ctx.clearRect(0, 0, clearX, clearY); // clear the canvas
        
        // if image is <= Canvas Size
        if (imgW <= CanvasXSize) {
            // reset, start from beginning
            if (x > CanvasXSize) {
                x = -imgW + x;
            }
            // draw additional image1
            if (x > 0) {
                ctx.drawImage(img, -imgW + x, y, imgW, imgH);
            }
            // draw additional image2
            if (x - imgW > 0) {
                ctx.drawImage(img, -imgW * 2 + x, y, imgW, imgH);
            }
        }
    
        // image is > Canvas Size
        else {
            // reset, start from beginning
            if (x > (CanvasXSize)) {
                x = CanvasXSize - imgW;
            }
            // draw aditional image
            if (x > (CanvasXSize-imgW)) {
                ctx.drawImage(img, x - imgW + 1, y, imgW, imgH);
            }
        }
        // draw image
        ctx.drawImage(img, x, y,imgW, imgH);
        // amount to move
        x += dx;
    }



    /*grooming*/
    var app= angular.module('myApp',[]);
app.controller('myController',function($scope){
    $scope.products=[
        {
            'tensp':'TẮM - SPA(1)',
            'soluong':0,
            'dongia':100,
            'isBuy':false
        },
        {
            'tensp':'VỆ SINH TAI(2)',
            'soluong':0,
            'dongia':35,
            'isBuy':false
        },
        {
            'tensp':'VỆ SINH - CẮT MÓNG(3)',
            'soluong':0,
            'dongia':45,
            'isBuy':false
        },
        {
            'tensp':'CẮT LÔNG VỆ SINH(4)',
            'soluong':0,
            'dongia':170,
            'isBuy':false
        },
        {
            'tensp':'CẮT LÔNG TẠO KIỂU(5)',
            'soluong':0,
            'dongia':350,
            'isBuy':false
        },
        {
            'tensp':'SILVER PACKAGE(1+2)',
            'soluong':0,
            'dongia':120,
            'isBuy':false
        },
        {
            'tensp':'TITAN PACKAGE(1+2+3)',
            'soluong':0,
            'dongia':160,
            'isBuy':false
        },
        {
            'tensp':'GOLD PACKAGE(1+2+3+4)',
            'soluong':0,
            'dongia':290,
            'isBuy':false
        },
        {
            'tensp':'DIAMOND PACKAGE(1+2+3+4+5)',
            'soluong':0,
            'dongia':450,
            'isBuy':false
        }
    ];

    $scope.tongtien=function(){
        var s=0;
        $scope.products.forEach(p =>{
        if(p.isBuy){
            s=s+p.soluong*p.dongia;
        }
        });
        return s; 
    }
    $scope.clickAll=function(){
        $scope.products.forEach(p =>{
            p.isBuy=!p.isBuy;
        })
    }
});
    app.filter('search',function(){
        return function(input, gia){
            var xuat=[];
        if(isNaN(gia)){
            xuat=input;
        }
        else{
            input.forEach(a =>{
            if(a.dongia>=gia){
                xuat.push(a);
            }
            });
        }
        return xuat;
        }
    });



    /*ĐĂNG KÝ THÔNG TIN HOTEL*/
    function kiem_tra(){ 
        var bool= true;
        //Kiểm tra mã sinh viên
    if(document.querySelector('#maSV').value.length==0){
        document.querySelector('#note_maSV').style.color="red";
        document.querySelector('#note_maSV').innerHTML="Bạn chưa nhập mã sinh viên";
        bool=false;
    }
    else if(document.querySelector('#maSV').value.length!=0){
        document.querySelector('#note_maSV').innerHTML="";
    }

    //Kiểm tra tên sinh viên
    if(document.querySelector('#name').value.length==0){
        document.querySelector('#note_name').style.color="red";
        document.querySelector('#note_name').innerHTML="Bạn chưa nhập tên sinh viên!";
         bool=false;
    }
    else if(document.querySelector('#name').value.length!=0){
        document.querySelector('#note_name').innerHTML="";
    }

    //Kiểm tra email sinh viên
    if(document.querySelector('#email').value.length==0){
        document.querySelector('#note_email').style.color="red";
        document.querySelector('#note_email').innerHTML="Bạn chưa nhập Email!";
         bool=false;
    }

    var x = document.querySelector('#email').value;
    var viTriA = x.indexOf("@");
    var viTriCham = x.lastIndexOf(".");
    
    if (viTriA < 1 || viTriCham < (viTriA + 2) || (viTriCham + 2) >= x.length) {
        document.querySelector('#note_email').style.color="red";
        document.querySelector('#note_email').innerHTML="Bạn nhập Email chưa đúng định dạng!";
        bool= false;
    }
    else if(document.querySelector('#email').value.length!=0){
        document.querySelector('#note_email').innerHTML="";
    }

    //Kiểm tra giới tính
    var bool_gender=false;
document.querySelectorAll('[name="gender"]').forEach(gender=>
{
    if(gender.checked){
        bool_gender=true;
        document.querySelector('#note_gender').innerHTML="";
    }
    if(bool_gender==false){
        document.querySelector('#note_gender').style.color="red";
        document.querySelector('#note_gender').innerHTML="Bạn chưa chọn giới tính!";
        bool=false;
    }
});
    //Kiểm tra quốc tịch
    if(document.querySelector('#country').value.length==0){
        document.querySelector('#note_country').style.color="red";
        document.querySelector('#note_country').innerHTML="Bạn chưa nhập quốc tịch!";
         bool=false;
    }
    else if(document.querySelector('#country').value.length!=0){
        document.querySelector('#note_country').innerHTML="";
    }

    //Kiểm tra sở thích
    var bool_hobby=false;
document.querySelectorAll('[name="hobby"]').forEach(hobby=>
{
    if(hobby.checked){
        bool_hobby=true;
        document.querySelector('#note_hobby').innerHTML="";
    }
    if(bool_hobby==false){
        document.querySelector('#note_hobby').style.color="red";
        document.querySelector('#note_hobby').innerHTML="Bạn chưa chọn sở thích!";
        bool=false;
    }
});

    //Kiểm tra ghi chú
    if(document.querySelector('#note').value.length==0||document.querySelector('#note').value.length>201){
        document.querySelector('#note_note').style.color="red";
        document.querySelector('#note_note').innerHTML="Ký tự nhập (1-200)ký tự!";
         bool=false;
    }
    else if(document.querySelector('#note').value.length!=0||document.querySelector('#note').value.length<=201){
        document.querySelector('#note_note').innerHTML="";
    }
    if(bool==true){
        alert("Thông tin của bạn đã được ghi nhận!");
    }
        return bool;

}




 


        

 










